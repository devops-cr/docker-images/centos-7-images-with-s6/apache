# centos7-apache

This is a docker images where we are running Apache Webserver in our already
created CentOS 7 image. We provide files to secure our webserver and enable SSL
if needed with the certificates we are passing to that image.

We have created environment variables where we can change the HTTP and HTTPS
port if we want to. Furthermore we have created two flags where we are enabling
security options on Apache and SSL connections if we want to. This flags are
`SECURITY_ENABLED` and `SSL_ENABLED` respectively.

In the httpd.conf file we have changed the values of the `ErrorLog` and `CustomLog`
to output their information into the stderr and stdout.

## Usage Examples

By default `SECURITY_ENABLED` and `SSL_ENABLED` are false which means we are not enabling them. 

 A simple example is the below one.

    docker build -t apache-test .
    docker run -d --name apache-container -p 8080:80 apache-test

In this example we are building the image and we are publishing the container's port 80 to the host's port 8080. Since we are not enabling SSL we don't need to publish port 443 to the host.

 Another example that we can use is the following one.

    docker build -t apache-test2 .
    docker run -d --name apache-container-safe -p 8080:80 9090:443 -e SSL_ENABLED=true -e SECURITY_ENABLED=true apache-test2

In this example we are creating a secure container and we are setting the `SSL_ENABLED` and `SECURITY_ENABLED` flags to true. 

 Finally we are providing a `docker-compose.yml` file where we are creating a docker container and we have set the `SSL_ENABLED` and `SECURITY_ENABLED` flags to true. We need to be careful with the image name of the container and name it accordingly. In our example the name we are using is `ansible-apache`

    docker build -t ansible-apache .
    docker-compose up -d
