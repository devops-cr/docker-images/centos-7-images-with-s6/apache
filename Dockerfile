FROM centos:7-christos

RUN yum install -y httpd \
    mod_ssl \
 && yum clean all

COPY etc/ /etc/
#COPY security_disabled /etc/httpd/conf.d/security.disabled

ARG HTTP_PORT=80 \
    HTTPS_PORT=443

ENV \
    SSL_ENABLED=false \
    SECURITY_ENABLED=false \
    HTTP_PORT=$HTTP_PORT \
    HTTPS_PORT=$HTTPS_PORT 
    



EXPOSE $HTTP_PORT $HTTPS_PORT

